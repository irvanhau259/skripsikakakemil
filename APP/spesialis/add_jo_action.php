<?php

include_once("../function/koneksi.php");
include_once("../function/helper.php");
session_start();
$id = $_SESSION['id'];

$artwork = implode(",", $_POST['artwork']);
$input_date = date('Y-m-d', strtotime($_POST['input_date']));
$due_date = date('Y-m-d', strtotime($_POST['due_date']));
$prod_name = $_POST['prod_name'];
$generic_name = $_POST['generic_name'];
$drug_id = $_POST['drug_id'];
$dosage_form = $_POST['dosage_form'];
$roa = $_POST['roa'];
$storage = $_POST['storage'];
$manufactured = $_POST['manufactured'];
$for = $_POST['for'];
$marketed = $_POST['marketed'];
$imported = $_POST['imported'];
$license = $_POST['license'];
$distributed = $_POST['distributed'];
$cc_number = $_POST['cc_number'];
$cc_detail = $_POST['cc_detail'];
$composition = $_POST['composition'];
$persentation = $_POST['persentation'];
$nie = $_POST['nie'];
$packaging_id = $_POST['packaging_id'];
$no_item = $_POST['no_item'];
$dimension = $_POST['dimension'];
$material = $_POST['material'];
$spesialis_id = $_POST['spesialis_id'];
$corrector_id = $_POST['corrector_id'];
$drafter_id = $_POST['drafter_id'];
$button = $_POST['button'];
$proses = 1;

if (isset($_POST['button'])) {
    // Insert Job Order
    $query = mysqli_query($koneksi, "INSERT into job_order (
            artwork_status,
            input_date,
            due_date,
            prod_name,
            generic_name,
            drug_id,
            dosage_form,
            roa,
            storage,
            manufactured,
            import,
            license,
            marketed,
            cc_number,
            cc_detail,
            compositon,
            presentation,
            nie,
            dimension,
            packaging_id,
            item_number,
            for_by,
            distributed_by,
            material,
            specialist_id,
            drafter_id, 
            corrector_id)
            values (
        '$artwork',
        '$input_date',
        '$due_date',
        '$prod_name',
        '$generic_name',
        '$drug_id',
        '$dosage_form',
        '$roa',
        '$storage',
        '$manufactured',
        '$imported',
        '$license',
        '$marketed',
        '$cc_number',
        '$cc_detail',
        '$composition',
        '$persentation',
        '$nie',
        '$dimension',
        '$packaging_id',
        '$no_item',
        '$for',
        '$distributed',
        '$material',
        '$spesialis_id',
        '$drafter_id',
        '$corrector_id')");
    // Jika Query sudah insert setelahnya akan menjalankan fungsi dibawah
    if ($query) {
        // Insert Proses
        $job_id = mysqli_insert_id($koneksi);
        $update_proses = "INSERT INTO proses(job_id,process_id)values('$job_id','$proses')";

        // Upload Images
        $targetDir = "../images/attachment/";
        //format pdf bisa di ganti ke format file lain seperti JPG, PNG, GIF
        $allowTypes = array('pdf', 'doc', 'docx', 'img', 'jpg', 'jpeg', 'png', 'ai');

        $insertValuesSQL = '';
        if (!empty(array_filter($_FILES['attachment']['name']))) {
            foreach ($_FILES['attachment']['name'] as $key => $val) {
                // tempat file upload
                $fileName = basename($_FILES['attachment']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                //ambil nilai dari form select
                $job_id = mysqli_insert_id($koneksi);
                $specialist_id = $_POST["spesialis_id"];

                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if (in_array($fileType, $allowTypes)) {
                    // unggah file ke directory
                    if (move_uploaded_file($_FILES["attachment"]["tmp_name"][$key], $targetFilePath)) {
                        //masukkan dalam $insertValuesSQL
                        $insertValuesSQL .= "('" . $job_id . "','" . $specialist_id . "','" . $fileName . "'),";
                    }
                }
            }
            if (!empty($insertValuesSQL)) {
                $insertValuesSQL = trim($insertValuesSQL, ',');
                // query insert mysql
                $insert = "INSERT INTO attachment (job_id, specialist_id, attachment) VALUES $insertValuesSQL";
                if (mysqli_query($koneksi, $insert)) {
                    echo "New Record Succesfully";
                } else {
                    echo "Error: " . $insert . "<br>" . mysqli_error($koneksi);
                }
            }
        }
        if (mysqli_query($koneksi, $update_proses)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $update_proses . "<br>" . mysqli_error($koneksi);
        }
    }
    echo "<script>
          window.location.href = 'dashboard.php?id=$id';
          alert('Job Order Berhasil Ditambahkan');
    </script>";
}
